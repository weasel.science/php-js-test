<?php

/**
 * Establish a PDO connection to MySQL.
 * Fetching the database data from the environment, to be provided by Cloud9.
 * Todo: use this connection to interface with the database.
 */
$db = new PDO(
    'mysql:host=' . getenv('IP') . ';dbname=c9',
    getenv('C9_USER'),
    ''
);

/**
 * Make sure our table exists before doing anything else.
 */
$db->exec('
    CREATE TABLE IF NOT EXISTS `todos` (
        `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `status` tinyint NOT NULL,
        `name` text NOT NULL
    ) COLLATE "utf8_unicode_ci";
');

/**
 * Perform an action based on the request type.
 * Todo: create and implement PATCH and DELETE handlers.
 */
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        /**
         * Todo: Return todo items encoded with JSON, containing all necessary
         * data so the client can render them.
         */
        echo "GET";
        break;
    case 'POST':
        /**
         * Todo: Accept a JSON-encoded body that contains all data necessary
         * to create a new todo item.
         * Use a URL parameter to identify the resource in question.
         * Make sure to use prepared statements to avoid injection attacks!
         */
        echo "POST";
        break;
}

?>